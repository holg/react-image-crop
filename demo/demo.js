import React from 'react';
import ReactDOM from 'react-dom';
import ReactCrop from '../lib/ReactCrop';

/**
 * Select an image file.
 */
var imageType = /^image\//;
var fileInput = document.querySelector('#file-picker');

fileInput.addEventListener('change', function(e) {
	var file = e.target.files.item(0);

	if (!file || !imageType.test(file.type)) {
		return;
	}

	var reader = new FileReader();

	reader.onload = function(e) {
		loadEditView(e.target.result);
	};

	reader.readAsDataURL(file);
});

/**
 * Load the image in the crop editor.
 */
var cropEditor = document.querySelector('#crop-editor');

function loadEditView(dataUrl) {
	
	var Parent = React.createClass({
		getInitialState: function() {
			return {
				crop: {
					x: 0,
					y: 0,
					aspect: 1/1,
					width: 50
				},
				img_dest:  new Image(),
                img_src: dataUrl
			};
		},

		onButtonClick: function() {
			this.setState({
				crop: {
					x: 20,
					y: 5,
					aspect: 1,
					width: 30,
					height: 50
				},
				img_dest : {}
			});
		},

		onImageLoaded: function(crop) {
			console.log('Image was loaded. Crop:', crop);
			this.setState({
				crop: {
					aspect: 1/1,
					width: 800,
				},
                img_dest : {}
			});
		},

		onCropComplete: function(crop) {
            this.setState({
                crop: crop});
			console.log('Crop move complete:', crop);
		},
		
		
		loadImage: function(src, callback) {
			var image = new Image();
			image.onload = function(e) {
				callback(image);
				image = null;
			};

			image.src = src;
		},

		
		cropImage: function() {
			var image = new Image();
            image.src = this.state.img_src;
			var imageWidth = image.naturalWidth;
			var imageHeight = image.naturalHeight;

			var cropX = (this.state.crop.x / 100) * imageWidth;
			var cropY = (this.state.crop.y / 100) * imageHeight;

			var cropWidth = (this.state.crop.width / 100) * imageWidth;
			var cropHeight = (this.state.crop.height / 100) * imageHeight;

			var canvas = document.createElement('canvas');
			canvas.width = cropWidth;
			canvas.height = cropHeight;
			var ctx = canvas.getContext('2d');

			ctx.drawImage(image, cropX, cropY, cropWidth, cropHeight, 0, 0, cropWidth, cropHeight);

			if (HTMLCanvasElement.prototype.toBlob) {
				console.info('It looks like Chrome now supports HTMLCanvasElement.toBlob.. time to uncomment some code!');
			}

			// canvas.toBlob will be faster and non-blocking but is currently only supported in FF.
			// canvas.toBlob(function(blob) {
			// 	var url = URL.createObjectURL(blob);

			// 	imgDest.onload = function() {
			// 		URL.revokeObjectURL(url);
			// 		this.ready();
			// 	};

			// 	imgDest.src = url;
			// });

            image.src = canvas.toDataURL('image/jpeg');
            this.setState({img_dest: image});
            loadEditView(image.src);
		},

		
		

		 onCropChange: function(crop) {
		 	console.log('Crop change');
		 },

		render: function() {
			return (
				<div>
					<ReactCrop
						crop={this.state.crop}
						src={dataUrl}
						onImageLoaded={this.onImageLoaded}
						onComplete={this.onCropComplete}
						disabled={false}
						onChange={this.onCropChange}
					/>
                    <button onClick={this.onButtonClick}>Programatically set crop</button>
					<button onClick={() => {this.setState({foo: Date.now()})}}>Change foo state</button>
					<button onClick={this.cropImage}>Crop to Canvas</button>
                    {/*<img*/}
						{/*src={this.img_dest.src}*/}
					{/*/>*/}
				</div>
			);
		}
	});

	ReactDOM.render(<Parent />, cropEditor);
}

